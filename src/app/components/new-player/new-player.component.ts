import { PlayerServiceService } from './../services/player-service.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Iplayer } from '../modules/iplayer';

@Component({
  selector: 'app-new-player',
  templateUrl: './new-player.component.html',
  styleUrls: ['./new-player.component.scss']
})
export class NewPlayerComponent implements OnInit {

  public superForm: FormGroup | null = null;
  // tslint:disable-next-line: no-inferrable-types
  public submitted: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private playerServiceService: PlayerServiceService
    ) {
    this.superForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(20)]],
      age: ['', [Validators.required, Validators.maxLength(20)]],
      position: ['', [Validators.required, Validators.maxLength(20)]],
      numero: ['', [Validators.required, Validators.maxLength(20)]],
      url: ['', [Validators.required, Validators.minLength(2)]],
    });
  }

  ngOnInit(): void {}

  public onSubmit(): void {
    this.submitted = true;

    if (this.superForm.valid) {
      const player: Iplayer = {
        name: this.superForm.get('name').value,
        age: this.superForm.get('age').value,
        position: this.superForm.get('position').value,
        numero: this.superForm.get('numero').value,
        url: this.superForm.get('url').value,
      };
      this.playerServiceService.postplayer(player);

      this.superForm.reset();
      this.submitted = false;
    }
  }

  public onActualizar(): void {}


}
