import { Iplayer } from './../modules/iplayer';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlayerServiceService {

  private player: Iplayer[];

  constructor() { 

    this.player = [
      {
        name: 'Giovanni Reyna',
        age: 18,
        position: 'Mediocentro ofensivo',
        numero: 32,
        url: 'https://s.hs-data.com/bilder/spieler/gross/425193.jpg?fallback=png',
      },
    ];
  }

  getplayer(): Iplayer[]{
    return this.player;
  }

  postplayer(player: Iplayer): void{
    this.player.push(player);
  }

  deletePlayer(nombre: string): void{
    this.player.forEach((jugador, i) => {    if ( nombre === jugador.name ) {
        this.player.splice(i, 1);
      }
    });
  }
}
