export interface Iplayer {

    name: string;
    age: number;
    position: string;
    numero: number;
    url: string;
}
