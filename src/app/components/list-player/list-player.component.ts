import { PlayerServiceService } from './../services/player-service.service';
import { Iplayer } from './../modules/iplayer';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-player',
  templateUrl: './list-player.component.html',
  styleUrls: ['./list-player.component.scss']
})
export class ListPlayerComponent implements OnInit {

  public myPlayers: Iplayer[] | null = null;
  constructor(private playerServiceService: PlayerServiceService) { }

  ngOnInit(): void {

    this.myPlayers = this.playerServiceService.getplayer();
  }

}
