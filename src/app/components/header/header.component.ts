import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  logo: string;
  name: string;


  constructor() {
    // tslint:disable-next-line: max-line-length
    this.logo = 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Borussia_Dortmund_logo.svg/1200px-Borussia_Dortmund_logo.svg.png';
    this.name = 'BORUSSIA DORTMUND';
  }

  ngOnInit(): void {
  }

}

