import { PlayerServiceService } from './components/services/player-service.service';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NewPlayerComponent } from './components/new-player/new-player.component';
import { ListPlayerComponent } from './components/list-player/list-player.component';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    NewPlayerComponent,
    ListPlayerComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
  ],
  providers: [ PlayerServiceService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
